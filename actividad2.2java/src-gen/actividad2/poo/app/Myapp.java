package actividad2.poo.app;

import java.time.LocalDate;

import actividad2.poo.clases.Expedientes;
import actividad2.poo.clases.Sintomas;

public class Myapp {
	
	static void run() {
		Sintomas sin = new Sintomas(37.5F, 150F, 72F);
		Expedientes re = new Expedientes("Pedro Picapiedra", "PPCD880786ADD", LocalDate.of(1994, 3, 7), sin);
		sin.aņadirSintomas("Dificultad al repirar");
		sin.aņadirSintomas("Fiebre");
		sin.aņadirSintomas("Dolor de cuerpo");
		sin.aņadirSintomas("Vomito");
		re.aņadirConsulta(LocalDate.of(2000, 4, 11));
		re.aņadirConsulta(LocalDate.of(2005, 6, 15));
		re.aņadirConsulta(LocalDate.of(2011, 9, 2));
		re.addDiagnostico("COVID-19");
		re.addReceta("No existe");
		System.out.println(re);
	}

	public static void main(String[] args) {
		run();
	}

}